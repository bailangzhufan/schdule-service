package com.dag.schedule.config.mybatis;


import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * mybatis-plus 配置
 */
@Configuration
@MapperScan("com.**.mapper*")
public class MybatisPlusConfig {

    /**
     * 事务管理,解决一个事务先写后读从库
     */
    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager (@Qualifier("customDataSource") CustomDataSource ownDataSource) {
        DataSourceManager dataSourceManager = new DataSourceManager();
        dataSourceManager.setDataSource (ownDataSource);
        return dataSourceManager;
    }

    /**
     * 逻辑删除
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }


    /**
     * 性能监控拦截器
     */
    @Bean
    @Profile({"dev","test"})// 设置 dev test 环境开启
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }


}
