package com.dag.schedule.config.mybatis;

/**
 * 切换数据源
 */
public final class DynamicHolder {

    private static final ThreadLocal<DynamicEnum> holder = new ThreadLocal<DynamicEnum>();

    private DynamicHolder() {}

    public static void putDataSource(DynamicEnum dataSource){
        holder.set(dataSource);
    }

    public static DynamicEnum getDataSource(){
        return holder.get();
    }

    public static void clearDataSource() {
        holder.remove();
    }

}
