package com.dag.schedule.config.mybatis;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 读写分离,目前只支持两个数据源
 * 根据需求添加数据源
 */
@Slf4j
@ConfigurationProperties(prefix = "dag.dataSource")
@Configuration
@Component("customDataSource")
public class CustomDataSource extends AbstractRoutingDataSource {

    private static boolean isMultipleDB;
    private DataSourceProperties master;
    private List<DataSourceProperties> slave;
    private List<DataSource> dataSources = new ArrayList<> ();
    private List<String> roundRobinList = new ArrayList<>();
    private Integer roundRobinIndex = 0;
    private final Object lock = new Object();


    // 初始化数据源
    private synchronized void initDataSources () {
        if(dataSources != null && dataSources.size() > 0){
            return;
        }
        if (master == null) {
            throw new IllegalArgumentException ("Master Database is necessary");
        }

        // 封装主数据源配置
        dataSources.add (buildDataSource(master));
        // 封装从数据源配置
        if(isMultipleDB = (slave != null && !slave.isEmpty())){
            for (DataSourceProperties slaveConfig : slave) {
                dataSources.add(buildDataSource(slaveConfig));
            }
        }

        // 设置默认数据源
        setDefaultTargetDataSource (this.dataSources.get (0));
        log.debug("主数据源" + DynamicEnum.MASTER.name () + "设置完毕.");

        // 设置所有目标数据源
        Map<Object, Object> targetDataSources = new HashMap<> ();
        targetDataSources.put (DynamicEnum.MASTER.name (), this.dataSources.get (0));
        int size = this.dataSources.size();
        for (int i = 1; i < size; i++) {
            String slaveName = DynamicEnum.SLAVE.name () + (i - 1);
            targetDataSources.put(slaveName, this.dataSources.get (i));
            roundRobinList.add(slaveName);
            log.debug("从数据源" + slaveName + "设置完毕.");
        }
        setTargetDataSources (targetDataSources);
        log.debug("初始化数据源配置完毕，共" + dataSources.size() + "个配置。");
    }

    // 构建数据源
    private HikariDataSource buildDataSource(DataSourceProperties dataSourceConfig){
        HikariDataSource dataSource = new HikariDataSource ();
        dataSource.setUsername (dataSourceConfig.getUsername ());
        dataSource.setPassword (dataSourceConfig.getPassword ());
        dataSource.setJdbcUrl (dataSourceConfig.getUrl ());
        dataSource.setDriverClassName (dataSourceConfig.getDriverClass ());
        Integer maxPoolSize = dataSourceConfig.getMaxPoolSize();
        if(maxPoolSize != null && maxPoolSize > 0){
            dataSource.setMaximumPoolSize(maxPoolSize);
        }
        Integer minPoolSize = dataSourceConfig.getMinPoolSize();
        if(minPoolSize != null && minPoolSize > 0){
            dataSource.setMinimumIdle(minPoolSize);
        }
        return dataSource;
    }


    // 设置多数据源
    @Override
    public void afterPropertiesSet () {
        // 初始化配置
        this.initDataSources();

        // 父类初始化(重要)
        super.afterPropertiesSet();
    }

    // 获取数据源
    @Override
    protected Object determineCurrentLookupKey () {
        DynamicEnum dynamicDataSourceGlobal = DynamicHolder.getDataSource ();
        // 非多数据源/默认数据源都是主数据源
        if (!isMultipleDB || dynamicDataSourceGlobal == null || dynamicDataSourceGlobal.equals(DynamicEnum.MASTER)) {
            log.debug("---------切换数据源：" + DynamicEnum.MASTER.name ());
            return DynamicEnum.MASTER.name ();
        }
        // 负载
        return this.slaveRoundRobin();
    }


    // 读库轮训
    private String slaveRoundRobin(){
        synchronized (lock) {
            if (roundRobinIndex >= roundRobinList.size()) {
                roundRobinIndex = 0;
            }
            String slaveName = roundRobinList.get(roundRobinIndex);
            log.debug("---------切换数据源：" + slaveName);
            roundRobinIndex++;
            return slaveName;
        }
    }

    // 是否多数据源
    public static boolean isIsMultipleDB(){
        return isMultipleDB;
    }

    public DataSourceProperties getMaster() {
        return master;
    }

    public void setMaster(DataSourceProperties master) {
        this.master = master;
    }

    public List<DataSourceProperties> getSlave() {
        return slave;
    }

    public void setSlave(List<DataSourceProperties> slave) {
        this.slave = slave;
    }

    public void setDataSources(List<DataSource> dataSources) {
        this.dataSources = dataSources;
    }

    public List<DataSource> getDataSources() {
        return dataSources;
    }
}