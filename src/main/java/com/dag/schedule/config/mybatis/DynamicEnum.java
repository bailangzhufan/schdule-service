package com.dag.schedule.config.mybatis;

/**
 * 数据源类型
 */
public enum DynamicEnum {
    SLAVE, MASTER;
}
