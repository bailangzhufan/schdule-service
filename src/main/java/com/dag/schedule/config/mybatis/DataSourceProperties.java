package com.dag.schedule.config.mybatis;

import lombok.Data;

/**
 * 数据源配置类
 */
@Data
public class DataSourceProperties {

    private String url;
    private String password;
    private String username;
    private String driverClass;
    private Integer maxPoolSize;
    private Integer minPoolSize;
}
