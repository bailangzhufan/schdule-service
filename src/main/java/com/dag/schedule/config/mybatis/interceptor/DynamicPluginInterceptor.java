package com.dag.schedule.config.mybatis.interceptor;

import com.dag.schedule.config.mybatis.CustomDataSource;
import com.dag.schedule.config.mybatis.DynamicEnum;
import com.dag.schedule.config.mybatis.DynamicHolder;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.keygen.SelectKeyGenerator;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {
                MappedStatement.class, Object.class}),
        @Signature(type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class,
                        CacheKey.class, BoundSql.class}),
        @Signature(type = Executor.class, method = "query", args = {
                MappedStatement.class, Object.class, RowBounds.class,
                ResultHandler.class})})
@Component
public class DynamicPluginInterceptor implements Interceptor {

    private static final String REGEX = ".*insert\\u0020.*|.*delete\\u0020.*|.*update\\u0020.*";
    private static final Map<String, DynamicEnum> cacheMap = new ConcurrentHashMap<> ();

    @Override
    public Object intercept (Invocation invocation) throws Throwable {

        // 不是多数据源走默认数据源
        if(!CustomDataSource.isIsMultipleDB()){
            return invocation.proceed ();
        }

        // 多数据源
        boolean synchronizationActive = TransactionSynchronizationManager.isSynchronizationActive ();
        if (!synchronizationActive) {
            Object[] objects = invocation.getArgs ();
            MappedStatement mappedStatement = (MappedStatement) objects[0];
            String mappedStatementId = mappedStatement.getId();

            DynamicEnum dynamicDataSourceGlobal = null;
            if ((dynamicDataSourceGlobal = cacheMap.get (mappedStatementId)) == null) {
                if (mappedStatement.getSqlCommandType ().equals (SqlCommandType.SELECT)) {
                    if (mappedStatementId.contains (SelectKeyGenerator.SELECT_KEY_SUFFIX)) {
                        dynamicDataSourceGlobal = DynamicEnum.MASTER;
                    } else {
                        BoundSql boundSql = mappedStatement.getSqlSource ().getBoundSql (objects[1]);
                        String sql = boundSql.getSql ().toLowerCase (Locale.CHINA).replaceAll ("[\\t\\n\\r]", " ");
                        if (sql.matches (REGEX)) {
                            dynamicDataSourceGlobal = DynamicEnum.MASTER;
                        } else {
                            dynamicDataSourceGlobal = DynamicEnum.SLAVE;
                        }
                    }
                } else {
                    dynamicDataSourceGlobal = DynamicEnum.MASTER;
                }
                cacheMap.put (mappedStatementId, dynamicDataSourceGlobal);
            }
            DynamicHolder.putDataSource (dynamicDataSourceGlobal);
        }
        return invocation.proceed ();
    }

    @Override
    public Object plugin (Object target) {
        if (target instanceof Executor) {
            return Plugin.wrap (target, this);
        } else {
            return target;
        }
    }

    @Override
    public void setProperties (Properties properties) {

    }
}
