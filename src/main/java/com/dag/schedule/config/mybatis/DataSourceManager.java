package com.dag.schedule.config.mybatis;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;

/**
 * 事物管理器
 */
public class DataSourceManager extends DataSourceTransactionManager {

    protected void doBegin (Object transaction, TransactionDefinition definition) {
        if (definition.isReadOnly ()){
            DynamicHolder.putDataSource (DynamicEnum.SLAVE);
        }else{
            DynamicHolder.putDataSource (DynamicEnum.MASTER);
        }
        super.doBegin (transaction, definition);
    }

    protected void doCleanupAfterCompletion (Object transaction) {
        super.doCleanupAfterCompletion (transaction);
    }
}
