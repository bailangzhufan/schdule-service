package com.dag.schedule.config;

/**
 * @author JFeng
 * @date 2019/1/11 9:44
 */
public class BaiduMapConstant {

    // 浏览器端AK
    public static final String WEB_AK = "2Bb5t6ET1sNFNkEaxzhRddIrtT8GMosP";

    // 服务端AK
    // public static final String SERVER_AK = "ibq7uCk1BIZC2fHHoDoThbXL3iZgZBVa";
    public static final String SERVER_AK = "8dGVmW28TKTRPwvNW3w4CG18VbXIvdUj";

    //驾车路线规划
    public static final String DRIVING_URL = "http://api.map.baidu.com/direction/v2/driving";

    //批量算路
    public static final String ROUTE_MATRIX_URL = "http://api.map.baidu.com/routematrix/v2/driving";

    // 地址解析
    public static final String GEO_CODER_URL = "http://api.map.baidu.com/geocoder/v2/";

    //IP定位
    public static final String IP_LOCATE_URL = "http://api.map.baidu.com/location/ip";

    //GPS定位
    public static final String GPS_LOCATE_URL = "https://api.map.baidu.com/locapi/v2";
}
