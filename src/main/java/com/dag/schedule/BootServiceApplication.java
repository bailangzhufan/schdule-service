package com.dag.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync(proxyTargetClass=true)
@EnableTransactionManagement
@EnableHystrix
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.dag"})
@SpringBootApplication(scanBasePackages = {"com.dag"})
public class BootServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootServiceApplication.class, args);
	}

}
