package com.dag.schedule.api.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author jian.sun
 * @date 2018/12/7
 */
@Data
public class DateDTO {

    private String strDate;
    private Date dateDate;

}
