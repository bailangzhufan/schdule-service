package com.dag.schedule.api.dto;

import com.dag.common.entity.BaseDTO;
import com.dag.common.utils.BeanValidatorsUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 示例代码
 */
@Data
public class DemoDTO extends BaseDTO {

    @NotNull(message ="jobName不能为空")
    private String jobName;


    /**
     *  重写此方法，自定义参数校验，可以写一些复杂校验
     *  1、可以自己手写校验
     *  2、使用工具类校验
     */
    @Override
    public void validate() {
        // 可以自己手写复杂校验
        /*if(jobName == null){
            throw new BusinessRuntimeException("jobName不能为空");
        }*/

        // 使用工具类校验
        BeanValidatorsUtils.validate(this);
    }
}
