package com.dag.schedule.service.impl;

import com.dag.schedule.common.business.SolutionBusiness;
import com.dag.schedule.utils.InitSolution3Data;
import com.dag.schedule.utils.InitSolutionData;
import com.dag.schedule.vehicleRouting.domain.Customer;
import com.dag.schedule.vehicleRouting.domain.Vehicle;
import com.dag.schedule.vehicleRouting.domain.VehicleRoutingSolution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VrpService {

    public static final String SOLVER_CONFIG = "solver/vehicleRoutingSolverConfig.xml";
    // public static final String SOLVER_CONFIG = "solver/vehicleRoutingSolverNearbyConfig.xml";


    /**
     * 初始化引擎（引擎加油）
     *
     * @return
     */
    public void init() {
        // 工厂方法模式 通用方法（ config problem）
        //初始化求解问题
        VehicleRoutingSolution solution = InitSolutionData.initBaseInfo();
        //初始化求解器---公共变量 public
        SolverFactory solverFactory = SolverFactory.createFromXmlResource(SOLVER_CONFIG);
        Solver solver = solverFactory.buildSolver();
        ScoreDirectorFactory scoreDirectorFactory = solver.getScoreDirectorFactory();
        ScoreDirector scoreDirector = scoreDirectorFactory.buildScoreDirector();
        //初始化求解器---局部变量 private
        SolutionBusiness solutionBusiness = new SolutionBusiness<VehicleRoutingSolution>();
        solutionBusiness.setSolver(solver);
        solutionBusiness.setScoreDirector(scoreDirector);
        solutionBusiness.setSolution(solution);
        //开始求解
        Object solve = solutionBusiness.solve(solution);
        getSolutionView((VehicleRoutingSolution) solve);
    }

    public void init2() {
        //初始化求解问题
        VehicleRoutingSolution solution = InitSolution3Data.initBaseInfo();
        //初始化求解器
        SolverFactory<VehicleRoutingSolution> solverFactory = SolverFactory.createFromXmlResource(SOLVER_CONFIG);
        Solver<VehicleRoutingSolution> solver = solverFactory.buildSolver();
        ScoreDirectorFactory<VehicleRoutingSolution> scoreDirectorFactory = solver.getScoreDirectorFactory();
        ScoreDirector<VehicleRoutingSolution> scoreDirector = scoreDirectorFactory.buildScoreDirector();

        SolutionBusiness<VehicleRoutingSolution> solutionBusiness = new SolutionBusiness<VehicleRoutingSolution>();
        solutionBusiness.setSolver(solver);
        solutionBusiness.setScoreDirector(scoreDirector);
        solutionBusiness.setSolution(solution);
        //开始求解
        VehicleRoutingSolution solveResult = solutionBusiness.solve(solution);
        getSolutionView(solveResult);
    }

    private void getSolutionView(VehicleRoutingSolution solveResult) {
        List<Vehicle> vehicleList = solveResult.getVehicleList();
        for (Vehicle vehicle : vehicleList) {
            Customer nextCustomer = vehicle.getNextCustomer();
            if (nextCustomer == null) {
                continue;
            }
            List<Customer> customers = new ArrayList<>();
            getCustomerName(nextCustomer, customers);
            StringBuilder sb = new StringBuilder();
            sb.append("[vehicle--"+vehicle.getId()+"  ("+vehicle.getCapacity()+")"+"]"+"配送线路:【START】");
            sb.append(vehicle.getDepot().getLocation().getName()+"--->");
            int totalDemand=0;
            for (Customer customer : customers) {
                sb.append(customer.getLocation().getName()).append("("+customer.getDemand()+")").append("--->");
                totalDemand+=customer.getDemand();
            }
            sb.append("【END】完成配送:"+totalDemand+" 单量:"+customers.size());
            System.out.println(sb.toString());
        }
    }

    public void getCustomerName(Customer customer, List<Customer> customers) {
        Customer nextCustomer = customer.getNextCustomer();
        if (nextCustomer != null) {
            getCustomerName(nextCustomer, customers);
        }
        customers.add(customer);
    }

    public static void main(String[] args) {
        VrpService vrpService = new VrpService();
        // TODO: 2019/3/6 问题创建求解异步进行---->>>>处理完成结果保存Redis---->>>>前端轮询Redis结果---->>>>处理渲染路线结果
        vrpService.init2();//上海问题
        // vrpService.init();//欧洲问题
    }


}

