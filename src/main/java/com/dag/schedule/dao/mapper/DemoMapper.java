package com.dag.schedule.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.schedule.dao.entity.Demo;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * 代码示例
 */
public interface DemoMapper extends BaseMapper<Demo> {

	List<Demo> selectList(Demo demo);

    List<Demo> selectList(Page page, Demo demo);
}
