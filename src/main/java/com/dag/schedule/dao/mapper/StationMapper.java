package com.dag.schedule.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.schedule.dao.entity.Demo;
import com.dag.schedule.dao.entity.Station;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * 代码示例
 */
public interface StationMapper extends BaseMapper<Station> {

    List<Station> selectStations();
}
