package com.dag.schedule.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author JFeng
 * @date 2019/3/4 16:55
 */
@Data
@TableName("metro_line_station")
public class Station {

    private Integer id;
    private String  metroStationName;
    private String  metroLineName;
    private String lat;
    private String lng;
    private String cityCode;
}
