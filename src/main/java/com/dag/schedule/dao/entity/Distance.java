package com.dag.schedule.dao.entity;

import lombok.Data;

/**
 * @author JFeng
 * @date 2018/12/18 15:01
 */
@Data
public class Distance {
    private String text;
    private Double value;
}
