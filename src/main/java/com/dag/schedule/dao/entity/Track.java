package com.dag.schedule.dao.entity;

import lombok.Data;

/**
 * 百度鹰眼轨迹点类
 * 注意: 此类中的字段命名对应百度字段管理中的字段名命名 方便转JSON
 * 可以添加自定义字段, 在网页上直接配置 http://lbsyun.baidu.com/trace/admin/service
 *
 * @author 孙建
 */
@Data
public class Track {

	/** 终端名称 */
	private String entityName;
	/** 经度 */
	private Double longitude;
	/** 纬度 */
	private Double latitude;
	/** 方向（0-359度） */
	private Integer direction;
	/** 高度（米） */
	private Double height;
	/** 时速（千米/时） */
	private Double speed;
	/** 楼层 */
	private String floor;
	/** 定位精度（米） */
	private Double radius;
	/** 定位时间(UNIX时间戳) */
	private Integer loc_time;
	

}
