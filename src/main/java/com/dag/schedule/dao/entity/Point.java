package com.dag.schedule.dao.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 业务点
 * @author JFeng
 * @date 2018/12/18 17:09
 */

@Data
public class Point  implements Serializable {
    //点计算坐标
    private Double[] coordinates;
    //类型   "Point"
    private String type="Point";
    //国家
    private String country;
    //省名
    private String province;
    //城市名
    private String city;
    //区县名
    private String district;
    //乡镇名
    private String town;
    //街道名（行政区划中的街道层级）
    private String street;
    //地标建筑物
    private String building;
    //详细地址
    private String address;
    //补充地址
    private String extendAddress;
}
