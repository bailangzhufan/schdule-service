package com.dag.schedule.dao.entity;

import lombok.Data;

/**
 * @author JFeng
 * @date 2019/1/10 15:17
 */
@Data
public class BaiduMapResult<T> {
    // 响应状态0：成功 1：服务内部错误 2：参数无效 7：无返回结果
    private int status;
    // 状态码对应的信息
    private String message;
    // 返回的结果
    private T result;
}
