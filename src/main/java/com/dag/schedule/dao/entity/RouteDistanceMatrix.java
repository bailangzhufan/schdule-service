package com.dag.schedule.dao.entity;

import lombok.Data;

/**
 * @author JFeng
 * @date 2018/12/18 14:57
 */
@Data
public class RouteDistanceMatrix {
    private Distance distance;
    private Duration duration;
}
