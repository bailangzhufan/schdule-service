package com.dag.schedule.dao.entity;

import lombok.Data;

import java.util.List;

/**
 * 距离矩阵
 * @author JFeng
 * @date 2018/12/18 14:56
 */
@Data
public class MatrixResult {
    private Integer status;
    private String message;
    private List<RouteDistanceMatrix> result;
}
