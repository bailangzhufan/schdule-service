package com.dag.schedule.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

/**
 * 示例代码
 * 注解TableName: 实体类对应的表名
 */
@Data
public class Demo extends DataEntity{

    // 任务名称
    private String jobName;
    // 表达式(优先级大于触发时间)
    private String cronExpress;
    // 触发时间
    private String triggerTime;
    // 回调功能简称
    private String functionName;
    // 回调url
    private String callbackUrl;
    // 回调参数
    private String callbackParams;
    // 回调状态 -2失败  -1取消   0等待  1暂停  2执行中  3成功
    private Integer callbackStatus;
    // 业务处理结果
    private String businessResult;

}
