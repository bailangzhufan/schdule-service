package com.dag.schedule.utils;

import net.sf.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

/**
 * 发送httpClient请求
 */
public class HttpClientUtils {
	
	private static Logger log = LoggerFactory.getLogger(HttpClientUtils.class);
	
	/**
	 * 使用POST方式发送HTTP请求
	 * @return 请求返回的参数
	 */
	public static  JSONObject httpPostByJson(String url, Map<String, Object> map) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost(url);
		HttpResponse response;
		Set<String> set = map.keySet();
		JSONObject jsonParam = new JSONObject();
		JSONObject jsonObject = null;
		String jsonStr ;
		
		try {
			for (String key : set) {
				jsonParam.put(key, map.get(key));
			}
			jsonStr = jsonParam.toString();
			
			StringEntity entity = new StringEntity(jsonStr, "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httppost.setEntity(entity);
			response = httpclient.execute(httppost);
			if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
				// 请求结束，返回结果
				String resData = EntityUtils.toString(response.getEntity());
				jsonObject = JSONObject.fromObject(resData);
			}
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jsonObject;
	}
	
	
	/**
	 * 使用GET方式发送HTTP请求
	 * @param url 请求
	 * @return 请求返回的参数
	 */
	public static  JSONObject httpGet(String url) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(url);
		HttpResponse response;
		JSONObject jsonObject = null;
		
		try {
			response = httpclient.execute(httpGet);
			// 请求结束，返回结果
			String resData = EntityUtils.toString(response.getEntity(),"utf-8");
			System.out.println(resData);
			getData(resData);
			jsonObject = JSONObject.fromObject(resData);

		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jsonObject;
	}
	/**
	 * 使用GET方式发送HTTP请求
	 * @param url 请求
	 * @return 请求返回的参数
	 */
	public static  String httpGetString(String url) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(url);
		HttpResponse response;

		try {
			response = httpclient.execute(httpGet);
			// 请求结束，返回结果
			String resData = EntityUtils.toString(response.getEntity(),"utf-8");
			return resData;

		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static void getData(String resData) {

	}

}
