package com.dag.schedule.utils;

import com.dag.common.entity.ViewPage;
import com.github.pagehelper.PageInfo;

/**
 * 分页封装工具类
 */
public final class PageUtils {

    private PageUtils(){}

    /**
     * 将框架分页对象封装为本项目分页对象
     */
    public static <T> ViewPage<T> convert(PageInfo<T> pageInfo){
        ViewPage viewPage = new ViewPage();
        viewPage.setRecords(pageInfo.getList());
        viewPage.setPageNum(pageInfo.getPrePage() + 1);
        viewPage.setPageSize(pageInfo.getPageSize());
        viewPage.setTotalPages(pageInfo.getPages());
        viewPage.setTotalCount(pageInfo.getTotal());
        return viewPage;
    }
}
