package com.dag.schedule.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.dag.schedule.config.BaiduMapConstant;
import com.dag.schedule.dao.entity.BaiduMapResult;
import com.dag.schedule.dao.entity.RouteDistanceMatrix;
import com.dag.schedule.vehicleRouting.domain.Customer;
import com.dag.schedule.vehicleRouting.domain.Depot;
import com.dag.schedule.vehicleRouting.domain.Vehicle;
import com.dag.schedule.vehicleRouting.domain.VehicleRoutingSolution;
import com.dag.schedule.vehicleRouting.domain.location.DistanceType;
import com.dag.schedule.vehicleRouting.domain.location.Location;
import com.dag.schedule.vehicleRouting.domain.location.RoadLocation;
import com.dag.schedule.vehicleRouting.domain.timewindowed.TimeWindowedCustomer;
import com.dag.schedule.vehicleRouting.domain.timewindowed.TimeWindowedDepot;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JFeng
 * @date 2019/3/3 16:55
 */
public class InitSolution2Data {
    public static final Integer customerListSize = 50;

    public static VehicleRoutingSolution initBaseInfo() {
        // Might be replaced by TimeWindowedVehicleRoutingSolution later on
        VehicleRoutingSolution solution = new VehicleRoutingSolution();
        //设置基础信息
        solution.setId(0L);
        solution.setName("name");
        solution.setDistanceType(DistanceType.ROAD_DISTANCE);
        solution.setDistanceUnitOfMeasurement("km");
        List<Location> customerLocationList = new ArrayList<>(customerListSize);
        //读取所有对象的坐标
        Map<Long, Location> locationMap = InitSolution2Data.initLocations(customerLocationList);
        solution.setLocationList(customerLocationList);
        //读取所有地址和距离矩阵
        InitSolution2Data.initMatrixDistance(customerLocationList);
        //构建枢纽和网点货量
        InitSolution2Data.initDepotAndCustomerList(solution, locationMap);
        //读取运力车辆资源
        InitSolution2Data.initVehicleList(solution, solution.getDepotList());
        return solution;
    }

    public static Map<Long, Location> initLocations(List<Location> customerLocationList) {
        List<String> locations = new ArrayList<>();
        locations.add("0	121.209207	31.248526	上海枢纽");
        locations.add("55	121.538472	31.311024	翔殷路");
        locations.add("110	121.516066	31.278922	鞍山新村");
        locations.add("165	121.475411	31.259035	中兴路");
        locations.add("220	121.492716	31.217413	陆家浜路");
        locations.add("275	121.50029	31.16671	杨思");
        locations.add("330	121.515133	31.089988	江月路");
        locations.add("385	121.362183	31.248368	丰庄");
        locations.add("440	121.430026	31.236733	隆德路");
        locations.add("495	121.469279	31.241917	自然博物馆");
        locations.add("550	121.488515	31.202606	世博会博物馆");
        locations.add("605	121.492716	31.217413	陆家浜路");
        locations.add("660	121.442314	31.201202	徐家汇");
        locations.add("715	121.375571	31.163862	星中路");
        locations.add("770	121.236257	31.110609	佘山");
        locations.add("825	121.235829	31.006987	醉白池");
        locations.add("880	121.42068	31.148312	锦江乐园");
        locations.add("935	121.45316	31.211335	衡山路");
        locations.add("990	121.474802	31.244699	新闸路");
        locations.add("1045	121.458503	31.285845	上海马戏城");
        locations.add("1100	121.444185	31.345991	呼兰路");
        locations.add("1155 121.307524 31.195432 徐泾东");
        locations.add("1210 121.393672 31.220484 威宁路");
        locations.add("1265 121.465932 31.23413 南京西路");
        locations.add("1320 121.533437 31.234854 世纪大道");
        locations.add("1375 121.608557 31.209932 金科路");
        locations.add("1430 121.535457 31.22605 锦绣路");
        locations.add("1485 121.492645 31.18033 长清路");
        locations.add("1540 121.456628 31.219964 常熟路");
        locations.add("1595 121.428339 31.262583 岚皋路");
        locations.add("1650 121.420161 31.309963 场中路");
        locations.add("1705 121.379313 31.350576 顾村公园");
        locations.add("1760 121.446217 31.414218 江杨北路");
        locations.add("1815 121.499387 31.376563 淞滨路");
        locations.add("1870 121.491636 31.311153 江湾镇");
        locations.add("1925 121.48291 31.257393 宝山路");
        locations.add("1980 121.419507 31.237991 金沙江路");
        locations.add("2035 121.444739 31.182978 漕溪路");
        locations.add("2090 121.51713 31.079655 联航路");
        locations.add("2145 121.487012 31.159351 东方体育中心");
        locations.add("2200 121.496122 31.207672 西藏南路");
        locations.add("2255 121.478011 31.248303 曲阜路");
        locations.add("2310 121.50817 31.280778 四平路");
        locations.add("2365 121.539677 31.301934 黄兴公园");
        locations.add("2420 121.593445 31.353883 外高桥保税区北");
        locations.add("2475 121.595327 31.296804 东靖路");
        locations.add("2530 121.579314 31.256659 云山路");
        locations.add("2585 121.533437 31.234854 世纪大道");
        locations.add("2640 121.516396 31.191286 高科西路");
        locations.add("2695 121.50168 31.154367 灵岩南路");


        Map<Long, Location> locationMap = new LinkedHashMap<>(customerListSize);
        for (int i = 0; i < customerListSize; i++) {
            //地址坐标
            Location location = new RoadLocation();
            String line = locations.get(i);
            String[] lineTokens = StringSplitUtils.splitBySpacesOrTabs(line.trim(), 3, 4);
            location.setId(Long.parseLong(lineTokens[0]));
            location.setLatitude(Double.parseDouble(lineTokens[1]));
            location.setLongitude(Double.parseDouble(lineTokens[2]));
            location.setName(lineTokens[3]);
            customerLocationList.add(location);
            locationMap.put(location.getId(), location);
        }
        return locationMap;
    }

    /**
     * 批量算路（VRP路线规划）
     * 计算驾车距离和时间  矩阵计算结果
     *
     * @param origins
     * @param destinations
     */
    public static List<RouteDistanceMatrix> getRouteDistanceMatrix(String origins, String destinations) {
        StringBuilder sb = new StringBuilder(BaiduMapConstant.ROUTE_MATRIX_URL);
        sb.append("?ak=" + BaiduMapConstant.SERVER_AK)
                .append("&origins=" + origins)
                .append("&destinations=" + destinations)
                .append("&output=json");
        String resultString = HttpClientUtils.httpGetString(sb.toString().replace("|", "%7C"));
        BaiduMapResult<List<RouteDistanceMatrix>> matrixResultBaiduMapResult = JSON.parseObject(resultString, new TypeReference<BaiduMapResult<List<RouteDistanceMatrix>>>() {
        });
        return matrixResultBaiduMapResult.getResult();
    }


    public static void initDepotAndCustomerList(VehicleRoutingSolution solution, Map<Long, Location> locationMap) {
        //枢纽
        List<Depot> depotList = new ArrayList<>(customerListSize);
        //客户
        List<Customer> customerList = new ArrayList<>(customerListSize);
        List<String> demandList = new ArrayList<>();
        demandList.add("0 0");
        demandList.add("55 15");
        demandList.add("110 16");
        demandList.add("165 31");
        demandList.add("220 14");
        demandList.add("275 33");
        demandList.add("330 7");
        demandList.add("385 9");
        demandList.add("440 17");
        demandList.add("495 33");
        demandList.add("550 26");
        demandList.add("605 18");
        demandList.add("660 10");
        demandList.add("715 32");
        demandList.add("770 26");
        demandList.add("825 14");
        demandList.add("880 32");
        demandList.add("935 33");
        demandList.add("990 30");
        demandList.add("1045 25");
        demandList.add("1100 18");
        demandList.add("1155 26");
        demandList.add("1210 2");
        demandList.add("1265 25");
        demandList.add("1320 10");
        demandList.add("1375 3");
        demandList.add("1430 28");
        demandList.add("1485 9");
        demandList.add("1540 21");
        demandList.add("1595 7");
        demandList.add("1650 24");
        demandList.add("1705 1");
        demandList.add("1760 5");
        demandList.add("1815 19");
        demandList.add("1870 4");
        demandList.add("1925 9");
        demandList.add("1980 7");
        demandList.add("2035 16");
        demandList.add("2090 27");
        demandList.add("2145 15");
        demandList.add("2200 17");
        demandList.add("2255 20");
        demandList.add("2310 27");
        demandList.add("2365 33");
        demandList.add("2420 18");
        demandList.add("2475 31");
        demandList.add("2530 19");
        demandList.add("2585 24");
        demandList.add("2640 16");
        demandList.add("2695 32");
        for (int i = 0; i < customerListSize; i++) {
            String line = demandList.get(i);
            Boolean timewindowed = false;
            String[] lineTokens = StringSplitUtils.splitBySpacesOrTabs(line.trim(), timewindowed ? 5 : 2);
            long id = Long.parseLong(lineTokens[0]);
            int demand = Integer.parseInt(lineTokens[1]);
            if (demand == 0) {
                Depot depot = timewindowed ? new TimeWindowedDepot() : new Depot();
                depot.setId(id);
                Location location = locationMap.get(id);
                if (location == null) {
                    throw new IllegalArgumentException("此枢纽 (" + id
                            + ")尚未录入坐标 (" + location + ").");
                }
                depot.setLocation(location);
                depotList.add(depot);
            } else {
                Customer customer = timewindowed ? new TimeWindowedCustomer() : new Customer();
                customer.setId(id);
                Location location = locationMap.get(id);
                if (location == null) {
                    throw new IllegalArgumentException("此枢纽 (" + id
                            + ") 尚未录入坐标 (" + location + ").");
                }
                customer.setLocation(location);
                customer.setDemand(demand);
                // Notice that we leave the PlanningVariable properties on null
                customerList.add(customer);
            }
        }

        solution.setCustomerList(customerList);
        solution.setDepotList(depotList);
    }

    public static void initVehicleList(VehicleRoutingSolution solution, List<Depot> depotList) {
        Integer vehicleListSize = 10;
        Integer capacity = 125;
        List<Vehicle> vehicleList = new ArrayList<>(vehicleListSize);
        long id = 0;
        for (int i = 0; i < vehicleListSize; i++) {
            Vehicle vehicle = new Vehicle();
            vehicle.setId(id);
            id++;
            vehicle.setCapacity(capacity);
            // Round robin the vehicles to a depot if there are multiple depots 1 2 多枢纽车辆均分
            vehicle.setDepot(depotList.get(i % depotList.size()));
            vehicleList.add(vehicle);
        }
        solution.setVehicleList(vehicleList);
    }

    public static void initMatrixDistance(List<Location> customerLocationList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < customerListSize; i++) {
            Location location = customerLocationList.get(i);
            if (i < customerListSize - 1) {
                sb.append(location.getLongitude()).append(",").append(location.getLatitude()).append("|");
            } else {
                sb.append(location.getLongitude()).append(",").append(location.getLatitude());
            }
        }
        String destinations = sb.toString();
        for (int i = 0; i < 3; i++) {
            String origins = customerLocationList.get(i).getLongitude() + "," + customerLocationList.get(i).getLatitude();
            List<RouteDistanceMatrix> routeDistanceMatrix = getRouteDistanceMatrix(origins, destinations);
            RoadLocation location = (RoadLocation) customerLocationList.get(i);
            Map<RoadLocation, Double> travelDistanceMap = new LinkedHashMap<>(customerListSize);
            for (int j = 0; j < customerListSize; j++) {
                if (i != j) {
                    String text = routeDistanceMatrix.get(j).getDistance().getText();
                    double travelDistance = Double.parseDouble(text.replace("公里", ""));
                    RoadLocation otherLocation = (RoadLocation) customerLocationList.get(j);
                    travelDistanceMap.put(otherLocation, travelDistance);
                }
            }
            location.setTravelDistanceMap(travelDistanceMap);
        }
    }

    public static void initMatrixDistance2(List<Location> customerLocationList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < customerListSize; i++) {
            Location location = customerLocationList.get(i);
            if (i < customerListSize - 1) {
                sb.append(location.getLongitude()).append(",").append(location.getLatitude()).append("|");
            } else {
                sb.append(location.getLongitude()).append(",").append(location.getLatitude());
            }
        }
        String destinations = sb.toString();
        List<List<String>> bigList = new ArrayList<>();
        for (int i = 18; i < customerListSize; i++) {
            String origins = customerLocationList.get(i).getLongitude() + "," + customerLocationList.get(i).getLatitude();
            List<RouteDistanceMatrix> routeDistanceMatrix = getRouteDistanceMatrix(origins, destinations);
            List<String> list = new ArrayList<>();
            for (int j = 0; j < customerListSize; j++) {
                String text = routeDistanceMatrix.get(j).getDistance().getText();
                String travelDistance;
                if (i != j) {
                    travelDistance = text.replace("公里", "").replace("米","");
                } else {
                    travelDistance = "0.00";
                }
                list.add(travelDistance);
            }
            System.out.println(list.toString().replace(",","").replace("[","").replace("]",""));
        }
    }

    public static void main(String[] args) {
        List<Location> customerLocationList = new ArrayList<>(customerListSize);
        initLocations(customerLocationList);
        initMatrixDistance2(customerLocationList);
    }
}
