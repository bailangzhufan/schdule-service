package com.dag.schedule.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JFeng
 * @date 2019/2/27 18:01
 */
public  class StringSplitUtils {

    // ************************************************************************
    // Split methods
    // ************************************************************************

    public static String[] splitBySpace(String line) {
        return splitBySpace(line, null);
    }

    public static String[] splitBySpace(String line, Integer numberOfTokens) {
        return splitBy(line, "\\ ", "a space ( )", numberOfTokens, false, false);
    }

    public static String[] splitBySpace(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "\\ ", "a space ( )", minimumNumberOfTokens, maximumNumberOfTokens, false, false);
    }

    public static String[] splitBySpace(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens,
                                 boolean trim, boolean removeQuotes) {
        return splitBy(line, "\\ ", "a space ( )", minimumNumberOfTokens, maximumNumberOfTokens, trim, removeQuotes);
    }

    public static String[] splitBySpacesOrTabs(String line) {
        return splitBySpacesOrTabs(line, null);
    }

    public static String[] splitBySpacesOrTabs(String line, Integer numberOfTokens) {
        return splitBy(line, "[\\ \\t]+", "spaces or tabs", numberOfTokens, false, false);
    }

    public static String[] splitBySpacesOrTabs(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "[\\ \\t]+", "spaces or tabs", minimumNumberOfTokens, maximumNumberOfTokens,
                false, false);
    }

    public static String[] splitByPipelineAndTrim(String line, int numberOfTokens) {
        return splitBy(line, "\\|", "a pipeline (|)", numberOfTokens, true, false);
    }

    public static String[] splitBySemicolonSeparatedValue(String line) {
        return splitBy(line, ";", "a semicolon (;)", null, false, true);
    }

    public static String[] splitBySemicolonSeparatedValue(String line, int numberOfTokens) {
        return splitBy(line, ";", "a semicolon (;)", numberOfTokens, false, true);
    }

    public static String[] splitByCommaAndTrim(String line, int numberOfTokens) {
        return splitBy(line, "\\,", "a comma (,)", numberOfTokens, true, false);
    }

    public static String[] splitByCommaAndTrim(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "\\,", "a comma (,)", minimumNumberOfTokens, maximumNumberOfTokens, true, false);
    }

    public static String[] splitBy(String line, String delimiterRegex, String delimiterName,
                            Integer numberOfTokens, boolean trim, boolean removeQuotes) {
        return splitBy(line, delimiterRegex, delimiterName, numberOfTokens, numberOfTokens, trim, removeQuotes);
    }

    public static String[] splitBy(String line, String delimiterRegex, String delimiterName,
                            Integer minimumNumberOfTokens, Integer maximumNumberOfTokens, boolean trim, boolean removeQuotes) {
        String[] lineTokens = line.split(delimiterRegex);
        if (removeQuotes) {
            List<String> lineTokenList = new ArrayList<>(lineTokens.length);
            for (int i = 0; i < lineTokens.length; i++) {
                String token = lineTokens[i];
                while ((trim ? token.trim() : token).startsWith("\"") && !(trim ? token.trim() : token).endsWith("\"")) {
                    i++;
                    if (i >= lineTokens.length) {
                        throw new IllegalArgumentException("The line (" + line
                                + ") has an invalid use of quotes (\").");
                    }
                    String delimiter;
                    switch (delimiterRegex) {
                        case "\\ ":
                            delimiter = " ";
                            break;
                        case "\\,":
                            delimiter = ",";
                            break;
                        default:
                            throw new IllegalArgumentException("Not supported delimiterRegex (" + delimiterRegex + ")");
                    }
                    token += delimiter + lineTokens[i];
                }
                if (trim) {
                    token = token.trim();
                }
                if (token.startsWith("\"") && token.endsWith("\"")) {
                    token = token.substring(1, token.length() - 1);
                    token = token.replaceAll("\"\"", "\"");
                }
                lineTokenList.add(token);
            }
            lineTokens = lineTokenList.toArray(new String[0]);
        }
        if (minimumNumberOfTokens != null && lineTokens.length < minimumNumberOfTokens) {
            throw new IllegalArgumentException("Read line (" + line + ") has " + lineTokens.length
                    + " tokens but is expected to contain at least " + minimumNumberOfTokens
                    + " tokens separated by " + delimiterName + ".");
        }
        if (maximumNumberOfTokens != null && lineTokens.length > maximumNumberOfTokens) {
            throw new IllegalArgumentException("Read line (" + line + ") has " + lineTokens.length
                    + " tokens but is expected to contain at most " + maximumNumberOfTokens
                    + " tokens separated by " + delimiterName + ".");
        }
        if (trim) {
            for (int i = 0; i < lineTokens.length; i++) {
                lineTokens[i] = lineTokens[i].trim();
            }
        }
        return lineTokens;
    }

    public static boolean parseBooleanFromNumber(String token) {
        switch (token) {
            case "0":
                return false;
            case "1":
                return true;
            default:
                throw new IllegalArgumentException("The token (" + token
                        + ") is expected to be 0 or 1 representing a boolean.");
        }
    }

}


