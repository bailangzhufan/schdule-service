package com.dag.schedule.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author JFeng
 * @date 2019/3/7 10:39
 */
public class ThredTest {
    private static  final int sum=12;
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> {
            try {
                Thread.sleep(2000);
                System.out.println("Thread run");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executorService.shutdown();//等待所有线程执行结束
        // executorService.shutdownNow();//立即中断所有线程
        System.out.println("Main run");
    }
}
