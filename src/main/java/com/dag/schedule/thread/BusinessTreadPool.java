package com.dag.schedule.thread;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

public class BusinessTreadPool {

    /**
     * 消息处理线程池
     */
    private static final int THREAD_POOL_INIT_SIZE = 4 * 2;//线程池初始大小
    private static final int THREAD_POOL_MAX_SIZE = 4 * 25;//线程池最大线程数
    private static final long THREAD_POOL_KEEP_ALIVE_TIME = 60;//线程最大空闲时间
    private static final TimeUnit THREAD_POOL_ALIVE_TIME_UNIT = TimeUnit.SECONDS;//线程空闲时间单位
    private static final int THREAD_POOL_BLOCK_QUEUE_SIZE = 4 * 500;//线程池阻塞队列的长度
    private static final BlockingQueue<Runnable> blockingQueue = new LinkedBlockingDeque<>(THREAD_POOL_BLOCK_QUEUE_SIZE);//线程空闲时间单位
    public static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(THREAD_POOL_INIT_SIZE, THREAD_POOL_MAX_SIZE
            , THREAD_POOL_KEEP_ALIVE_TIME, THREAD_POOL_ALIVE_TIME_UNIT, blockingQueue);


    // int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue

    // 4 * 2, 4 * 2 , 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()
    private static final int THREAD_POOL_SIZE =Runtime.getRuntime().availableProcessors();//线程池大小
    private static final ThreadFactory factory = new ThreadFactoryBuilder().setDaemon(true).setNameFormat("org.reflections-scanner-%d").build();
    public static final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE, factory);

    //0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>()
    public static final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    //1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()
    public static final ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    //corePoolSize, Integer.MAX_VALUE, 0, NANOSECONDS, new DelayedWorkQueue()
    public static final ExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);




}