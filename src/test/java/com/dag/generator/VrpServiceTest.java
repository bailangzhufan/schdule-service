package com.dag.generator;

import com.dag.schedule.BootServiceApplication;
import com.dag.schedule.service.impl.VrpService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author JFeng
 * @date 2019/3/4 17:12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BootServiceApplication.class)
@Slf4j
public class VrpServiceTest {
    @Autowired
    VrpService vrpService;

    @Test
    public void init() {
        vrpService.init();
    }
}